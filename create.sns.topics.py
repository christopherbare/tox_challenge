import boto
import boto.sns
import sys

sys.path.append('/Users/chris/ec2/')
import ec2

sns = boto.sns.snsect_to_region('us-east-1', aws_access_key_id=ec2.key, aws_secret_access_key=ec2.secret)

response = sns.create_topic('tox-challenge-submission')
tcs_arn = response['CreateTopicResponse']['CreateTopicResult']['TopicArn']

response = sns.create_topic('tox-challenge-error')
tce_arn = response['CreateTopicResponse']['CreateTopicResult']['TopicArn']


sns.subscribe(tcs_arn, 'email', 'toxchallenge@sagebase.org')
sns.subscribe(tce_arn, 'email', 'toxchallenge@sagebase.org')

## For some reason, this doesn't work. Have to subscribe in the AWS console.
## sns.subscribe(tce_arn, 'SMS', '1-206-963-5407')

## DisplayName has to be set in order to send SMS messages
sns.set_topic_attributes(tce_arn, 'DisplayName', 'Tox Challenge Errors')
sns.set_topic_attributes(tcs_arn, 'DisplayName', 'Tox Challenge Submissions')

sns.publish(tcs_arn, 'This is a test message for the Tox Challenge Submission topic on SNS.', 'Test message for Tox Challenge Submission topic')
sns.publish(tce_arn, 'This is a test error message! Foo!!', 'Foo error')
