##
##
############################################################
library(synapseClient)

## your login credentials here:
#synapseLogin('','')

source('uuid.R')

eid <- '1968121'
evaluation <- synGetEvaluation(eid)

## guess randomly
n <- 3
m <- 5
guess <- matrix(log(rnorm(n*m)^2+1.0, base=2), nrow=n, ncol=m)

entity <- File(name='A silly random guess 2', parentId='syn1968004' )
entity <- addObject(entity, guess)
entity <- synStore(entity)

submission <- submit(evaluation, entity)
