import boto
import boto.sns
import json
import sys

sys.path.append('/Users/chris/ec2/')
import ec2

## get message info as JSON from STDIN
message_info = json.loads(sys.stdin.read())
if not('topic' in message_info and 'subject' in message_info and 'body' in message_info):
    raise Exception('send_notification.py takes JSON as input with these 3 fields: topic, subject, and body')

print 'Notifying %s' % message_info['topic']

if message_info.get('testing', False):
    print 'TESTING: No notification sent.'
    exit(0)

sns = boto.sns.connect_to_region('us-east-1', aws_access_key_id=ec2.key, aws_secret_access_key=ec2.secret)

## topic ARNs
arns = {
    'tox-challenge-submission':'arn:aws:sns:us-east-1:794329705108:tox-challenge-submission',
    'tox-challenge-error':'arn:aws:sns:us-east-1:794329705108:tox-challenge-error'
}

if message_info['topic'] not in arns:
    raise Exception('No such topic: %s' % message_info['topic'])

sns.publish(
    arns[message_info['topic']],
    message_info['body'],
    message_info['subject'])
