## Sample code to submit an entry to the
## DREAM Toxicogenetics Challenge
## subchallenge 1
############################################################
import synapseclient
from synapseclient import File, Folder, Project, Evaluation
import numpy as np
import random
import string
from math import log


def read_matrix(filepath):
    """
    Read 2D numpy array from a tab delimited file.

    :param filepath:

    :returns: a tuple containing a 2D numpy array, a list of row_names,
              and a list of column names
    """
    with open(filepath) as f:
        col_names = f.readline().strip().split('\t')
        dt = np.dtype([('rowname', np.str_, 8),
                       ('data', np.float, (len(col_names),))])
        rows = np.genfromtxt(f, delimiter='\t', dtype=dt)
    row_names = rows['rowname']
    data = np.vstack( [d['data'] for d in rows] )

    return (data, row_names, col_names)

def write_matrix(filepath, matrix, row_names, col_names):
    """
    Write a tab-delimited file from a numpy array with 2 dimensions.

    :param matrix: an n by m numpy array
    :param row_names: a list of row names of length n
    :param col_names: a list of column names of length m
    """
    with open(filepath, 'w') as f:
        f.write('\t'.join(col_names))
        f.write('\n')
        for row in range(matrix.shape[0]):
            f.write(row_names[row])
            f.write('\t')
            f.write('\t'.join([str(x) for x in matrix[row,:]]))
            f.write('\n')

## generate a random guess
def generate_guess(n,m):
    """
    Generate a random n by m matrix as a numpy array

    :param n: number of rows
    :param m: number of columns

    :returns: an n by m numpy array
    """
    guess = np.empty([n,m])
    for i in range(0,n):
        for j in range(0,m):
            guess[i,j] = log( random.gauss(mu=0.0, sigma=1.0)**2 + 1.0, 2 )
    return guess


syn = synapseclient.Synapse()
syn.login() ## add your credentials here, or make a .synapseConfig file

## retrieve ToxSubchallenge_1_Leaderboard_Submission_File_Format.txt
example_entity = syn.get('syn1917707')
example_matrix, row_names, col_names = read_matrix(example_entity.path)

guess = generate_guess(example_matrix.shape[0], example_matrix.shape[1])

write_matrix('guess.txt', guess, row_names, col_names)

## random tag to uniquify project name
tag = ''.join([random.choice(string.ascii_lowercase + string.digits) for i in range(10)])

## create a Synapse project to hold a submission
project = syn.store(Project('Test challenge submission %s' % tag))

## store the random guess in a Synapse project
entity = File('guess.txt', parent=project)
entity = syn.store(entity)

## evaluation ID for DREAM Toxicogenetics subchallenge 1 = '1917695'
evaluation = syn.getEvaluation('1917695')

syn.submit(evaluation, entity, name='Next guess', teamName='Team Gauss')
