#Scoring Harness for Tox Challenge

Validate submissions to the challenge, compute rankings and generate leader-boards.

NIEHS-NCATS-UNC DREAM Toxicogenetics Challenge ([syn1761567](https://www.synapse.org/#!Synapse:syn1761567))



### TODO


* file lock to prevent multiple scorers running
* send info about set-up to Xa, Bruce and ???
* notifications for me - to google group or AWS-SNS
* prevent duplicate submissions
* enforce rules
* what to do when there are too many entries? top 100, and full leaderboard? multiple pages?
* monitoring app - number and fate of submissions
* total unique teams

* how to create provenance in Synapse

