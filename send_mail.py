import boto.ses
import sys
import ec2
import json

mail_info = json.loads(sys.stdin.read())

if not('to' in mail_info and 'from' in mail_info and 'subject' in mail_info and 'body' in mail_info):
    raise Exception('send_mail.py takes JSON as input with these 4 fields: to, from, subject, and body')

print 'To:', mail_info['to']
print 'From:', mail_info['from']
print 'Subject:', mail_info['subject']
print '-' * 60
print mail_info['body']

if mail_info.get('testing', False):
    print 'TESTING: No email sent.'
    exit(0)

conn = boto.ses.connect_to_region('us-east-1', aws_access_key_id=ec2.key, aws_secret_access_key=ec2.secret)

## verified email address
## result = conn.verify_email_address('toxchallenge@sagebase.org')

conn.send_email(
  mail_info['from'],
  mail_info['subject'],
  mail_info['body'],
  [mail_info['to']])

print 'sent email to %s' % mail_info['to']
